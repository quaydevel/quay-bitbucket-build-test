FROM scratch

WORKDIR /app
COPY . .

CMD ["bash", "/app/run.sh"]
